<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContdatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contdatas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->string('cont_id');
            $table->string('amount');
            $table->unique(array('user_id','cont_id'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contdatas');
    }
}
