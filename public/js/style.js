function clearSession() {
    localStorage.removeItem('token');
    localStorage.removeItem('auth');
}