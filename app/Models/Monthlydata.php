<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Monthlydata extends Model
{
    use HasFactory;
    
    protected  $fillable=['user_id','monthly_id','amount','transid','sms','saved'];

   
}
