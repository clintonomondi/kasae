<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtpLogs extends Model
{
    protected  $fillable=['phone','code'];
}
