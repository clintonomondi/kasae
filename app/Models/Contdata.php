<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Contdata extends Model
{
    use HasFactory;
    
    protected  $fillable=['user_id','cont_id','amount','transid'];

    
}
