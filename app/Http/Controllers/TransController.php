<?php

namespace App\Http\Controllers;

use App\Money;
use App\Monthlydata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransController extends Controller
{
    public  function getMonthlyTrans(){
        $datas = DB::select( DB::raw("SELECT id,amount,created_at,
(SELECT name FROM users B WHERE B.id=A.user_id)user_name,
(SELECT email FROM users D WHERE D.id=A.user_id)phone,
(SELECT name FROM monthlies C WHERE C.id=A.monthly_id)month_name
 FROM `monthlydatas` A ORDER BY user_name ASC") );

        return ['status'=>true,'data'=>$datas];
    }

    public  function editMonthly(Request $request){
        if($request->user()->role!='secretary'){
            return ['status'=>false,'message'=>'You are not authorised'];
        }
        $request->validate([
            'amount' => 'required',
            'created_at' => 'required',
            'id' => 'required'
        ]);
        $amount2=$request->amount;
        $data=Monthlydata::find($request->id);
        $money=Money::find(1);
        $request['amount']=($money->amount-$data->amount)+$request->amount;
        $money->update($request->all());
        $data->amount=$amount2;
        $data->created_at=$request->created_at;
        $data->save();
        return ['status'=>true,'message'=>'Transaction updated successfully'];
    }

    public  function getCumulativeTrans(){

        $datas = DB::select( DB::raw("SELECT id,name,
(SELECT if(SUM(amount) is null,'0',SUM(amount)) FROM `monthlydatas` B WHERE B.user_id=A.id)amount
 FROM `users` A ORDER BY name ") );
        return ['status'=>true,'data'=>$datas];
    }
}
