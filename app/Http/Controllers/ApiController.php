<?php

namespace App\Http\Controllers;

use App\Models\Cont;
use App\Models\Contact;
use App\Models\Money;
use App\Models\Monthlydata;
use App\Models\User;
use App\Models\Expense;
use Alert;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    public  function getSummery(){
        $users=User::where('status','ACTIVE')->count();
        $issues=Cont::count();
        $saving1=Monthlydata::sum('amount');
        $saving2=Monthlydata::sum('saved');
        $saving=$saving1+$saving2;
        $expense=Expense::sum('amount');
        $expenses= DB::select( DB::raw("SELECT * FROM expenses ORDER BY id DESC") );
        $query = DB::select( DB::raw("SELECT (SELECT IF(SUM(amount) is null,'50',SUM(amount)) FROM monthlydatas WHERE MONTH(created_at) = 1 )january,
			 		(SELECT IF(SUM(amount) is null,'50',SUM(amount)) FROM monthlydatas WHERE MONTH(created_at) = 2 )february,
			 		(SELECT IF(SUM(amount) is null,'50',SUM(amount)) FROM monthlydatas WHERE MONTH(created_at) = 3 )march,
			 		(SELECT IF(SUM(amount) is null,'50',SUM(amount)) FROM monthlydatas WHERE MONTH(created_at) = 4 )april,
			 		(SELECT IF(SUM(amount) is null,'50',SUM(amount)) FROM monthlydatas WHERE MONTH(created_at) = 5 )may,
			 		(SELECT IF(SUM(amount) is null,'50',SUM(amount)) FROM monthlydatas WHERE MONTH(created_at) = 6 )june,
			 		(SELECT IF(SUM(amount) is null,'50',SUM(amount)) FROM monthlydatas WHERE MONTH(created_at) = 7 )july,
			 		(SELECT IF(SUM(amount) is null,'50',SUM(amount)) FROM monthlydatas WHERE MONTH(created_at) = 8 )augast,
			 		(SELECT IF(SUM(amount) is null,'50',SUM(amount)) FROM monthlydatas WHERE MONTH(created_at) = 9 )september,
			 		(SELECT IF(SUM(amount) is null,'50',SUM(amount)) FROM monthlydatas WHERE MONTH(created_at) = 10 )october,
			 		(SELECT IF(SUM(amount) is null,'50',SUM(amount)) FROM monthlydatas WHERE MONTH(created_at) = 11 )november,
			 		(SELECT IF(SUM(amount) is null,'50',SUM(amount)) FROM monthlydatas WHERE MONTH(created_at) = 12 )december,
			 		(SELECT COUNT(*) FROM users WHERE gender='Male')male,
			 		(SELECT COUNT(*) FROM users WHERE gender='Female')female,
			 		(SELECT COUNT(*) FROM users WHERE gender!='Male' AND gender!='Female')others
			 		FROM DUAL"));
        $data=Array(
            'issues'=>$issues,
            'members'=>$users,
            'savings'=>$saving,
            'expense'=>$expense,
        );
        $monthly=[
            $query[0]->january,
            $query[0]->february,
            $query[0]->march,
            $query[0]->april,
            $query[0]->may,
            $query[0]->june,
            $query[0]->july,
            $query[0]->augast,
            $query[0]->september,
            $query[0]->october,
            $query[0]->november,
            $query[0]->december,
            ];
        $gender=[
            $query[0]->male,
            $query[0]->female,
            $query[0]->others,
        ];
        return ['status'=>true,'data'=>$data,'series'=>$monthly,'gender'=>$gender,'expenses'=>$expenses];
    }

    public  function postContact(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
            'name'=>'required',
            'subject'=>'required',
            'message'=>'required',
        ]);

        $data=Contact::create($request->all());
        return ['status'=>true,'message'=>'Your message has been submitted successfully'];
    }

    public  function getContact(){
       $messages=Contact::orderBy('status','desc')->get();
       return ['status'=>true,'data'=>$messages];
    }

    public  function mark(Request $request,$id){
        $data=Contact::find($id);
        $request['status']='Read';
        $data->update($request->all());
        return back();
    }

    public  function team(){
        $users=User::all();
        return view('members.team',compact('users'));
    }


    public  function postPic(Request $request,$id){
        if(Auth::user()->role!='Secretary'){
            return ['status'=>false,'message'=>'Oops!You are not authorised!'];
        }
        $request->validate([
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
        ]);
        //get file name with extension
        $fileNameWithExt=$request->file('pic')->getClientOriginalName();
        $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
        $extesion=$request->file('pic')->getClientOriginalExtension();
        //filename to store
        $fileNameToStore=$filename.'_'.time().'.'.$extesion;
        //uploadimage
        $path=$request->file('pic')->storeAs('/public/avatars',$fileNameToStore);

        $user=User::find($id);
        Storage::delete('/public/avatars/'.$user->pic);
        $user->pic=$fileNameToStore;
        $user->save();
       return ['status'=>true,'message'=>'Profile pic updated successfully'];
    }
}
