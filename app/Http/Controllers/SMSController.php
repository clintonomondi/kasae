<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Knox\AFT\AFT;
use Illuminate\Support\Facades\Auth;


class SMSController extends Controller
{
    public  function postAllsms(Request $request){
        if(Auth::user()->role!='Secretary'){
            return ['status'=>false,'message'=>'You are not authorised'];
        }
        $validatedData = $request->validate([
            'message' => 'required',
        ]);
        $request['type']='All';
        $data=Message::create($request->all());

        foreach ($request->email as $item){
            $phone = "254" . substr($item, 1);
            AFT::sendMessage($phone, $request->message);


        }

       return ['status'=>true,'message'=>'Messages sent successfully to selected members.'];
    }
    public  function getAllsms(){
     $data=Message::orderBy('id','desc')->get();
        return $data;
    }
}
