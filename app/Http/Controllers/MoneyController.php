<?php

namespace App\Http\Controllers;

use App\Models\Cont;
use App\Models\Contdata;
use App\Models\Money;
use App\Models\Monthly;
use App\Models\Monthlydata;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Alert;
use Illuminate\Support\Facades\DB;
use Knox\AFT\AFT;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Services\PayUService\Exception;
use App\Mail\GetOtp;

class MoneyController extends Controller
{
    public  function contributions(){
        $conts = DB::select( DB::raw("SELECT *,
 (SELECT sum(amount) from contdatas B WHERE B.cont_id=A.id)amounts
 FROM conts A ORDER BY id DESC") );
        return ['status'=>true,'data'=>$conts];
    }

    public  function postCont(Request $request){
        if($request->user()->role=='member'){
            return ['status'=>false,'message'=>'You are not authorised'];
        }
        $validatedData = $request->validate([
            'name' => 'required',
            'amount' => 'required',
        ]);
        $data=Cont::create($request->all());
        return ['status'=>true,'message'=>'Contribution added successfully'];
    }

    public  function moreSingle($id){
        $cont=Cont::find($id);
        $total = DB::table('contdatas')->where('cont_id',$id)->sum('amount');

        $contdata = DB::select( DB::raw("SELECT id,name,email,
 (SELECT sum(amount) FROM contdatas B WHERE B.user_id=A.id AND B.cont_id='$id')amount,
 (SELECT transid FROM contdatas B WHERE B.user_id=A.id AND B.cont_id='$id' LIMIT 1)transid
 FROM users A") );
        return ['status'=>true,'cont'=>$cont,'contdata'=>$contdata,'total'=>$total];
    }


    public  function postSingleRecord(Request $request){
        if(Auth::user()->role!='Secretary'){
            return ['status'=>false,'message'=>'Oops!You are not authorised!'];
        }
        $request->validate([
            'amount' => 'required|numeric',
        ]);
        try {
            $cont=Cont::find($request->cont_id);
            $user=User::find($request->user_id);
            $phone = "254" . substr($user->email, 1);
            $data = Contdata::create($request->all());
            $amount=Contdata::where('cont_id',$request->cont_id)->sum('amount');
            AFT::sendMessage($phone, 'Hi ' . $user->name . ', we received your payment of Ksh.' . $request->amount . ' for  ' . $cont->name .'.Total collected is Ksh.'. $amount.'.Thank you.@KASAE UNITED!');
        } catch (QueryException $exception) {
            return ['status'=>false,'message'=>'The member has a record towards this contribution'];
        }
        return ['status'=>true,'message'=>'Record made successfully'];
    }

    public  function postsmsSingle(Request $request){
        if(Auth::user()->role!='Secretary'){
            return ['status'=>false,'message'=>'Oops!You are not authorised!'];
        }
        $user=User::find($request->user_id);
        $cont=Cont::find($request->cont_id);
        $phone = "254" . substr($user->email, 1);
        AFT::sendMessage($phone, 'Hi ' . $user->name . ', Please send your contribution  to 0708114058  for ' . $cont->name . '.@KASAE UNITED!');
       
        try{
            if(!empty($user->email2) && $user->email2!==null){
                $request['message']='Hi ' . $user->name . ', Please send your contribution  to 0708114058 -Phina Osano  for ' . $cont->name . '.@KASAE UNITED!';
                $request['subject']='KASAE UNITED-'.$cont->name.' CONTRIBUTIONS';
            Mail::to($user->email2)->send(new GetOtp($request));
            }
           } catch (\Exception $e) {
           
           }
        return ['status'=>true,'message'=>'Message sent successfully to '.$user->name];
    }

}
