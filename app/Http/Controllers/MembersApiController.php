<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Money;
use App\Cont;
use App\Contdata;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Knox\AFT\AFT;
use Illuminate\Support\Facades\Auth;

class MembersApiController extends Controller
{
    public  function getMembers(){
		  $members=DB::select( DB::raw("SELECT *, (SELECT SUM(amount) FROM monthlydatas B WHERE B.user_id=A.id)amount FROM `users` A WHERE status='ACTIVE' ORDER BY name desc") );
        return ['status'=>true,'data'=>$members];
    }
   
    public function getMemberData($id){
     $info=User::find($id);
     $cont = DB::table('contdatas')->where('user_id',$id)->sum('amount');
     $monthly = DB::table('monthlydatas')->where('user_id',$id)->sum('amount');
     return ['status'=>true,'info'=>$info,'cont'=>$cont,'monthly'=>$monthly];
    }

    public function updateMember(Request $request,$id){
        if(Auth::user()->role!='Secretary'){
            return ['status'=>false,'message'=>'You are not authorised'];
        }
        $validatedData = $request->validate([
            'email' => 'required|max:10|min:10',
            'name' => 'required',
            'gender' => 'required',
            'role' => 'required',
        ]);
        $user=User::find($id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'Member information updated succesffully'];
    }

    public  function submitMember(Request $request){
        
        if(Auth::user()->role!='Secretary'){
            return ['status'=>false,'message'=>'Oops!You are not authorised!'];
        }
        $validatedData = $request->validate([
            'email' => 'required|unique:users|max:10|min:10',
            'name' => 'required',
            'gender' => 'required',
        ]);
        $request['password']=bcrypt('adminuytytyfhf65438779786');
        $request['code']='KU'.mt_rand(1000,9999);
        $user=User::create($request->all());
        $phone ="254".substr($request->email, 1);
        AFT::sendMessage($phone, 'Congratulations! '.$request->name.'.You have been registered as a member of KASAE UNITED.Memebr ID is '.$request->code.'.@KASAE UNITED');
       return ['status'=>true,'message'=>'New member registered successfully'];
    }

    public  function role(){
        $users=User::all();
        return $users;
    }
}
