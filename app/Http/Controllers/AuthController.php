<?php

namespace App\Http\Controllers;

use App\Mail\GetOtp;
use App\Models\Money;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Knox\AFT\AFT;
use Illuminate\Support\Facades\Mail;
use App\Services\PayUService\Exception;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);        $user = new User([
        'name' => $request->name,
        'email' => $request->email,
        'password' => bcrypt($request->password)
    ]);        $user->save();        return response()->json([
        'message' => 'Successfully created user!'
    ], 201);
    }


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return ['status'=>false,'message' => 'Invalid phone or password'];

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);

        return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();        return response()->json([
        'message' => 'Successfully logged out'
    ]);
    }

    public function user(Request $request)
    {
        $members=User::count();
        $monthly=Money::sum('amount');
        return response()->json(['status'=>true,'data'=>$request->user(),'members'=>$members,'monthly'=>$monthly]);
    }

    public  function getOTP(Request $request){
        $datas = User::where('email',$request->email)->first();
        if($datas==null){
            return ['status'=>false,'message'=>'The phone number does not exist.Please contact office.'];
        }
        $randomid = mt_rand(1000,9999);
        $request['password']=bcrypt($randomid);
   
        $phone ="254".substr($request->email, 1);
        AFT::sendMessage($phone, 'Your OTP password is '.' '.$randomid.'.@KASAE UNITED');
        $user=User::where('id',$datas->id)->update(['password'=>$request->password]);
        
        try{
        if(!empty($datas->email2) && $datas->email2!==null){
            $request['message']='Hi,'.$datas->name.',Your One Time Password is '.$randomid.',please use this password to login';
            $request['subject']='One Time Password';
        Mail::to($datas->email2)->send(new GetOtp($request));
        }
       } catch (\Exception $e) {
       
       }
        return  ['status'=>true,'user'=>$datas,'message'=>'A four digit  code has been sent to your phone and email'];
    }
public  function verifyOtp(Request $request){
    $datas = DB::select( DB::raw("SELECT * FROM `otp_logs` WHERE phone='$request->phone' AND code='$request->code' AND created_at > NOW() - INTERVAL 4 HOUR") );
    if($datas==null){
        return ['status'=>false,'message'=>'Invalid code'];
    }
    return ['status'=>true,'message'=>'success'];
}

public  function resetPassword(Request $request){
    $validatedData = $request->validate([
        'password' => 'required',
        'repass' => 'required',
    ]);
    if($request->password!=$request->repass){
        return ['status'=>false,'message'=>'The password are not matching'];
    }
    $password=bcrypt($request->password);
    $datas = DB::select( DB::raw("UPDATE users SET password='$password' WHERE email='$request->phone'") );
    return ['status'=>true,'message'=>'Password reset successfully login'];
}

}
