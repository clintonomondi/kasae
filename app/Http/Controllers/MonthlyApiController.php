<?php

namespace App\Http\Controllers;

use App\Contdata;
use App\Models\Cont;
use App\Models\Expense;
use App\Models\Money;
use App\Models\Monthly;
use App\Models\Monthlydata;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Knox\AFT\AFT;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Services\PayUService\Exception;
use App\Mail\GetOtp;

class MonthlyApiController extends Controller
{
    public  function contributions(){
        $conts = DB::select( DB::raw("SELECT *,
 (SELECT sum(amount) from monthlydatas B WHERE B.monthly_id=A.id)amounts,
 (SELECT sum(saved) from monthlydatas B WHERE B.monthly_id=A.id)saved
 FROM monthlies A ORDER BY id DESC") );
        $sum =Money::select('amount')->where('id',1)->first();
        return ['status'=>false,'data'=>$conts,'sum'=>$sum];
    }
    public  function postMonthly(Request $request){
        if($request->user()->role=='member'){
            return ['status'=>false,'message'=>'You are not authorised'];
        }
        $validatedData = $request->validate([
            'name' => 'required',
            'amount' => 'required',
        ]);
        $data=Monthly::create($request->all());
      return ['status'=>true,'message'=>'New Month started successfully'];
    }

    public  function moreMonthly($id){
        $cont=Monthly::find($id);
        $t1 = DB::table('monthlydatas')->where('monthly_id',$id)->sum('amount');
        $t2 = DB::table('monthlydatas')->where('monthly_id',$id)->sum('saved');
        $total=$t1+$t2;

        $contdata = DB::select( DB::raw("SELECT id,name,email,
       (SELECT sum(amount) FROM monthlydatas B WHERE B.user_id=A.id AND B.monthly_id='$id')amount,
       (SELECT sum(saved) FROM monthlydatas B WHERE B.user_id=A.id AND B.monthly_id='$id')saved,
       (SELECT transid FROM monthlydatas B WHERE B.user_id=A.id AND B.monthly_id='$id' LIMIT 1)transid
        FROM users A WHERE status='ACTIVE'"));
        return ['status'=>true,'cont'=>$cont,'contdata'=>$contdata,'total'=>$total];
    }
    public  function postMonthlyRecord(Request $request){
        if(Auth::user()->role!='Secretary'){
            return ['status'=>false,'message'=>'Oops!You are not authorised!'];
        }
        $request->validate([
            'amount' => 'required|numeric',
        ]);
        if($request->amount<200){
            return ['status'=>false,'message'=>'Amount cannot be less than ksh 200']; 
        }
        try {
            $amt=$request->amount;
            $amount_to_play=$request->amount-200-10;
            $request['amount']=200;       
            $user=User::find($request->user_id);
            $cont=Monthly::find($request->cont_id);
            $phone = "254" . substr($user->email, 1);
            if($amount_to_play>=1){
                $request['saved']=$amount_to_play;
            }
            $request['sms']=10; 
            $data = Monthlydata::create($request->all());
            $request['saving']=$user->saving+$amount_to_play;
            
            $saver=User::where('id',$request->user_id)->update(['saving'=>$request->saving]);
            $user2=User::find($request->user_id);

            $expense=Expense::sum('amount');
            $contributions=Monthlydata::sum('amount');
            $group=$contributions-$expense;
            

           AFT::sendMessage($phone, 'Hi ' . $user2->name . ', we received your payment of Ksh.' . $amt . ' for  ' . $cont->name . '.Your savings is Ksh.'.$user2->saving.'.Group savings is Ksh.'.$group.'.@KASAE UNITED!');
        } catch (QueryException $exception) {
           return ['status'=>false,'message'=>'The member has a record towards this contribution'];
        }
        return ['status'=>true,'message'=>'Record made successfully'];
    }

    public  function postsmsMonthly(Request $request){
        if(Auth::user()->role!='Secretary'){
            return ['status'=>false,'message'=>'Oops!You are not authorised!'];
        }
        $user=User::find($request->user_id);
        $cont=Monthly::find($request->cont_id);
        $phone = "254" . substr($user->email, 1);
        AFT::sendMessage($phone, 'Hi ' . $user->name . ', Please send your contribution of Ksh.' . $cont->amount . ' to 0708114058   for the month of ' . $cont->name . '.@KASAE UNITED!');
       
        try{
            if(!empty($user->email2) && $user->email2!==null){
                $request['message']='Hi ' . $user->name . ', Please send your contribution of Ksh.' . $cont->amount . ' to 0708114058   for the month of ' . $cont->name . '.@KASAE UNITED!';
                $request['subject']='KASAE UNITED-MONTHLY CONTRIBUTION';
            Mail::to($user->email2)->send(new GetOtp($request));
            }
           } catch (\Exception $e) {
           
           }
       
       
        return ['status'=>true,'message'=>'Message sent successfully to '.$user->name];
    }
}
