<!DOCTYPE html>
<html lang="en">
<head>
	<title>Kasae United</title>

	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="THE KASAE UNITED "/>
    <meta property="og:image" content="/images/logo.png"/>
    <meta property="og:description" content="Kasae United was started before 1900.This is transformation or innovation of what was started by our forefathers into a group of youths who have common goal."/>

 <!-- Favicons -->
    <link href="/images/logo.png" rel="icon">
    <link href="/images/logo.png" rel="apple-touch-icon">

	<!-- Google Font -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;700&family=Roboto:wght@400;500;700&display=swap">

	<!-- Plugins CSS -->
	<link rel="stylesheet" type="text/css" href="/assets/vendor/font-awesome/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/vendor/bootstrap-icons/bootstrap-icons.css">
	<link rel="stylesheet" type="text/css" href="/assets/vendor/tiny-slider/tiny-slider.css">
	<link rel="stylesheet" type="text/css" href="/assets/vendor/glightbox/css/glightbox.css">

	<!-- Theme CSS -->
	<link  rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<link  rel="stylesheet" type="text/css" href="/css/login.css">

</head>

<body>
	<div class="content2">
    <div id="app">
        <app></app>
    </div>
	</div>
    <script src="{{ mix('js/app.js') }}"></script>



	<!-- =======================
		Footer START -->
		<footer class="pt-5">
			<div class="container">
				<!-- Row START -->
				<div class="row g-4">
		
					
		
		
					
				</div><!-- Row END -->
		
				<!-- Divider -->
				<hr class="mt-4 mb-0">
		
				<!-- Bottom footer -->
				<div class="py-3">
					<div class="container px-0">
						<div class="d-md-flex justify-content-between align-items-center py-3 text-center text-md-left">
							<!-- copyright text -->
							<div class="text-primary-hover"> Copyrights <a href="index.html#" class="text-body">©2022 Kasae United</a>. All rights reserved. </div>
							<!-- copyright links-->
							<div class=" mt-3 mt-md-0">
								<ul class="list-inline mb-0">
									<li class="list-inline-item">
										<!-- Language selector -->
										<div class="dropup mt-0 text-center text-sm-end">
											<a class="dropdown-toggle nav-link" href="index.html#" role="button" id="languageSwitcher" data-bs-toggle="dropdown" aria-expanded="false">
												<i class="fas fa-globe me-2"></i>Language
											</a>
											<ul class="dropdown-menu min-w-auto" aria-labelledby="languageSwitcher">
												<li><a class="dropdown-item me-4" href="#"><img class="fa-fw me-2" src="assets/images/flags/uk.svg" alt="">English</a></li>
												<li><a class="dropdown-item me-4" href="#"><img class="fa-fw me-2" src="assets/images/flags/gr.svg" alt="">Kiswahili </a></li>
												<li><a class="dropdown-item me-4" href="#"><img class="fa-fw me-2" src="assets/images/flags/sp.svg" alt="">French</a></li>
											</ul>
										</div>
									</li>
									<li class="list-inline-item"><a class="nav-link" href="/constitution">Our constitution</a></li>
								
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- =======================
		Footer END -->


<!-- Back to top -->
<div class="back-top"><i class="bi bi-arrow-up-short position-absolute top-50 start-50 translate-middle"></i></div>

<!-- Bootstrap JS -->
<script src="/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

<!-- Vendors -->
<script src="/assets/vendor/tiny-slider/tiny-slider.js"></script>
<script src="/assets/vendor/glightbox/js/glightbox.js"></script>
<script src="/assets/vendor/purecounterjs/dist/purecounter_vanilla.js"></script>

<!-- Template Functions -->
<script src="/assets/js/functions.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="/loader/center-loader.js"></script>
<script src="/js/style.js"></script>



<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
</body>
</html>