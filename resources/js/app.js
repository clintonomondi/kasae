import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input';
import Notifications from 'vue-notification';
import vueCountryRegionSelect from 'vue-country-region-select';
import JsonExcel from "vue-json-excel";
import moment from 'moment';
import VueGeolocation from 'vue-browser-geolocation';
import JwPagination from 'jw-vue-pagination';
import VueTimeago from 'vue-timeago'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import vSelect from "vue-select";
import "vue-select/dist/vue-select.css";
import Ads from 'vue-google-adsense'
import CoolLightBox from 'vue-cool-lightbox'
import 'vue-cool-lightbox/dist/vue-cool-lightbox.min.css'
import shareIt from 'vue-share-it';
import VueLoading from 'vuejs-loading-plugin'
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'


Vue.use(VueFormWizard);
Vue.use(VueLoading)
Vue.use(shareIt);
Vue.use(require('vue-script2'));
Vue.use(CoolLightBox);

Vue.use(Ads.Adsense);
Vue.use(Ads.InArticleAdsense);
Vue.use(Ads.InFeedAdsense);

Vue.component("v-select", vSelect);

Vue.use(VueToast);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(vueCountryRegionSelect);
Vue.component("downloadExcel", JsonExcel);
Vue.use(VueGeolocation);
Vue.component('jw-pagination', JwPagination);
Vue.component("v-select", vSelect);
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
});

import App from './view/App'
import Index from './pages/index'
import Members from './pages/members'
import Monthly from './pages/monthly'
import Monthlydata from './pages/monthlydata'
import Harambee from './pages/harambee'
import Harambeedata from './pages/harambeedata'
import Login from './pages/login'
import SMS from './pages/sms'
import MembersList from './pages/memberslist'
import Const from './pages/cons'


const router = new VueRouter({
    mode: 'history',
    routes: [

        {
            path: '/',
            name: 'index',
            component: Index,
        },
        {
            path: '/sms',
            name: 'sms',
            component: SMS,
        },
        {
            path: '/constitution',
            name: 'constitution',
            component: Const,
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/home',
            name: 'home',
            component: Index,
        },
        {
            path: '/members',
            name: 'members',
            component: Members,
        },
        {
            path: '/members/list',
            name: 'memberslist',
            component: MembersList,
        },
        {
            path: '/monthly',
            name: 'monthly',
            component: Monthly,
        },
        {
            path: '/harambee',
            name: 'harambee',
            component: Harambee,
        },
        {
            path: '/monthly/:id',
            name: 'monthlydata',
            component: Monthlydata,
        },
        {
            path: '/harambeedata/:id',
            name: 'harambeedata',
            component: Harambeedata,
        },


    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});