<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('signup', 'App\Http\Controllers\AuthController@signup');
    Route::post('getOTP', 'App\Http\Controllers\AuthController@getOTP');
    Route::post('verifyOtp', 'App\Http\Controllers\AuthController@verifyOtp');
    Route::post('resetPassword', 'App\Http\Controllers\AuthController@resetPassword');


    Route::get('logout', 'App\Http\Controllers\AuthController@logout');
        Route::get('user', 'App\Http\Controllers\AuthController@user');

		Route::get('/getMembers', 'App\Http\Controllers\MembersApiController@getMembers');
Route::get('/getSummery', 'App\Http\Controllers\ApiController@getSummery');
Route::get('/getMemberData/{id}', 'App\Http\Controllers\MembersApiController@getMemberData');

Route::get('/role', 'App\Http\Controllers\MembersApiController@role');

Route::get('/contributions', 'App\Http\Controllers\MonthlyApiController@contributions');
Route::get('/moreMonthly/{id}', 'App\Http\Controllers\MonthlyApiController@moreMonthly');
Route::post('/postMonthly', 'App\Http\Controllers\MonthlyApiController@postMonthly');



Route::get('/getSingle', 'App\Http\Controllers\MoneyController@contributions');
Route::post('/postCont', 'App\Http\Controllers\MoneyController@postCont');
Route::get('/moreSingle/{id}', 'App\Http\Controllers\MoneyController@moreSingle');

Route::get('/getContact', 'App\Http\Controllers\ApiController@getContact');
Route::get('/mark/{id}', 'App\Http\Controllers\ApiController@mark');
Route::post('/postContact', 'App\Http\Controllers\ApiController@postContact');

Route::get('/getMonthlyTrans', 'App\Http\Controllers\TransController@getMonthlyTrans');
Route::post('/editMonthly', 'App\Http\Controllers\TransController@editMonthly');
Route::get('/getCumulativeTrans', 'App\Http\Controllers\TransController@getCumulativeTrans');

Route::get('/getAllsms', 'App\Http\Controllers\SMSController@getAllsms');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::post('/updateMember/{id}', 'App\Http\Controllers\MembersApiController@updateMember');
        Route::post('/postPic/{id}', 'App\Http\Controllers\ApiController@postPic');
        Route::post('/postsmsMonthly', 'App\Http\Controllers\MonthlyApiController@postsmsMonthly');
        Route::post('/postMonthlyRecord', 'App\Http\Controllers\MonthlyApiController@postMonthlyRecord');
        Route::post('/postsmsSingle', 'App\Http\Controllers\MoneyController@postsmsSingle');
        Route::post('/postSingleRecord', 'App\Http\Controllers\MoneyController@postSingleRecord');
        Route::post('/submitMember', 'App\Http\Controllers\MembersApiController@submitMember');


       Route::post('/postAllsms', 'App\Http\Controllers\SMSController@postAllsms');
        
    });
});

